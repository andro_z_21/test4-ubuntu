import socket
import zlib

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('0.0.0.0', 8080))
s.listen(50)
while True:
	clt, adr = s.accept()
	request = clt.recv(2048)
	reply = b'Hi, This is your reply for -> ["' + request + b'"] <- this message!'
	if len(request) > 0:
		clt.send(reply)